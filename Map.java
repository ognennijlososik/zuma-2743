public class Map {
    public Point zeroPoint;

    private static final float ALPHA = 10f;
    private static final float BETA = 0.17f;

    private float movingSpeed;


    public Map(float movingSpeed) {
        this.movingSpeed = movingSpeed;
        float radius = 300;
        float theta = (float) (Math.log(radius / ALPHA) / BETA);
        zeroPoint = new Point(theta, radius);
    }

    public Point getPoint(float x) {
        float radius = (float) (zeroPoint.getRadius() - x * BETA / Math.sqrt(1 + BETA * BETA));
        float theta = (float) (Math.log(radius / ALPHA) / BETA);
        return new Point(theta, radius);
    }

    public void setLoseSpeed() {
        movingSpeed = 10;
    }

    public void move() {
        zeroPoint = getPoint(movingSpeed);
    }
}
