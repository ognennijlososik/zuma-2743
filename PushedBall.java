import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

public class PushedBall {
    public static final float RADIUS = 20.0f;
    public static final int width = GameBoard.BOARD_WIDTH;
    public static final int height = GameBoard.BOARD_HEIGHT;
    public static ArrayList<PushedBall> pushedBalls;
    public static SimpleColor simpleColor;
    public static Map map;

    public final Color color;

    private double angle;
    private Ellipse2D ellipse2D;

    private float x;
    private float y;

    private int pos;
    private float target;

    private boolean moving = false;

    private static final int pushedSpeed = 10;

    public PushedBall(double angle, final Color color) {
        this.angle = angle;
        this.color = color;
        x = width / 2;
        y = height / 2;
        refresh();
    }

    public Ellipse2D getEllipse2D() {
        return ellipse2D;
    }

    public void move() {
        if (moving) {
            Point targetPoint = map.getPoint(target);
            double critical = 3;
            if (Math.hypot(x - targetPoint.getX(), y - targetPoint.getY()) < critical) {
                x = targetPoint.getX();
                y = targetPoint.getY();
                MovingBall.movingBalls.add(pos, new MovingBall(pos, color));
                pushedBalls.remove(this);
                int first = pos;
                int last = pos;
                while (first > 0 && MovingBall.movingBalls.get(first - 1).color.equals(color)) {
                    first--;
                }
                while (MovingBall.movingBalls.size() - 1 > last &&
                        MovingBall.movingBalls.get(last + 1).color.equals(color)) {
                    last++;
                }

                if (last - first >= 2) {
                    for (int i = first; i <= last; i++) {
                        MovingBall.movingBalls.remove(first);
                    }
                    for (int i = first; i < MovingBall.movingBalls.size(); i++) {
                        MovingBall.movingBalls.get(i).setMove(false, last - first + 1);
                    }
                    boolean dropColor = true;
                    for (MovingBall movingBall : MovingBall.movingBalls) {
                        if (movingBall.color.equals(color)) {
                            dropColor = false;
                            break;
                        }
                    }
                    if (dropColor) {
                        simpleColor.inGame.remove(color);
                        if (simpleColor.nextBallColor.equals(color)) {
                            simpleColor.updateNextBallColor();
                        }
                    }
                }

            } else {
                int vectorX = targetPoint.getX() - x >= 0 ? 1 : -1;
                int vectorY = targetPoint.getY() - y >= 0 ? 1 : -1;
                x += vectorX * 3;
                y += vectorY * 3;

            }
        } else {
            double r = Math.hypot(x - width / 2, y - height / 2) + pushedSpeed;
            x = (float) (width / 2 + r * Math.cos(angle));
            y = (float) (height / 2 + r * Math.sin(angle));
            double critical = 20;
            boolean moveForward = false;
            for (int i = 0; i < MovingBall.movingBalls.size(); i++) {
                MovingBall ball = MovingBall.movingBalls.get(i);
                if (moveForward) {
                    ball.setMove(true, 1);
                } else {
                    if (Math.hypot(ball.getPoint().getX() - x,
                            ball.getPoint().getY() - y) < critical) {
                        moveForward = true;
                        moving = true;
                        pos = i + 1;
                        target = pos * MovingBall.RADIUS;
                    }
                }
            }
        }
        refresh();
    }

    private void refresh() {
        ellipse2D = new Ellipse2D.Float(x, y, RADIUS, RADIUS);
    }
}
