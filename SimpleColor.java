import java.awt.*;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class SimpleColor {
    public Set<Color> inGame = new HashSet<Color>();
    public Color nextBallColor;
    private Color[] colors = new Color[]{
            Color.RED, Color.GREEN, Color.BLUE, Color.MAGENTA, Color.CYAN
    };
    private Random random = new Random();
    private int total;

    public SimpleColor(int total) {
        this.total = total;
    }

    public Color getRandomColor() {
        int r = random.nextInt(total);
        return colors[r];
    }

    public void updateNextBallColor() {
        nextBallColor = getInGameColor();
    }

    private Color getInGameColor() {
        if (inGame.size() == 0) {
            return null;
        }
        int r = random.nextInt(inGame.size());
        int i = 0;
        for (Color c : inGame) {
            if (i == r) {
                return c;
            }
            i = i + 1;
        }
        return null;
    }

}