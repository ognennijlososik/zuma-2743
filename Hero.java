import java.awt.*;

public class Hero extends Polygon {

    int gBWidth = GameBoard.BOARD_WIDTH;
    int gBHeight = GameBoard.BOARD_HEIGHT;

    private int heroWidth = 27;
    private int heroHeight = 30;

    private double centerX = gBWidth / 2 + heroWidth / 2;
    private double centerY = gBHeight / 2 + heroHeight / 2;

    public static int[] polyXArray = {-13, 14, -13, -5, -13};
    public static int[] polyYArray = {-15, 0, 15, 0, -15};

    private double rotationAngle = 0;

    public Hero() {
        super(polyXArray, polyYArray, 5);
    }

    public double getXCenter() {
        return centerX;
    }

    public double getYCenter() {
        return centerY;
    }

    public int getHeroWidth() {
        return heroWidth;
    }

    public int getHeroHeight() {
        return heroHeight;
    }

    public double getRotationAngle() {
        return rotationAngle;
    }

    public void increaseRotationAngle() {
        if (getRotationAngle() >= 355) {
            rotationAngle = 0;
        } else {
            rotationAngle += 5;
        }
    }

    public void decreaseRotationAngle() {
        if (getRotationAngle() < 0) {
            rotationAngle = 355;
        } else {
            rotationAngle -= 5;
        }
    }
}