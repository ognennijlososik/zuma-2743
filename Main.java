public class Main {
    public static void main(String[] args) {
        double a = 2;
        for (double r = 50; r > 5; r -= 0.1d) {
            double theta = r / a;
            double x = a * theta * Math.cos(theta);
            double y = a * theta * Math.sin(theta);
            System.out.println(x + " " + y);
        }
    }
}
