import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

public class MovingBall {
    public static final float RADIUS = 20.0f;
    public static final int width = GameBoard.BOARD_WIDTH;
    public static final int height = GameBoard.BOARD_HEIGHT;
    public static ArrayList<MovingBall> movingBalls;
    public static Map map;

    public final Color color;

    private float x;
    private int pos;

    private boolean moving = false;
    private float target;
    private float speed = 3;

    public MovingBall(final int n, final Color color) {
        x = n * RADIUS;
        target = x;
        pos = n;
        this.color = color;
    }

    public Ellipse2D getEllipse2D() {
        if (moving) {
            float temp;
            if (target > x) {
                temp = x + speed;
            } else {
                temp = x - speed;
            }
            if (Math.abs(target - temp) < Math.abs(target - x)) {
                x = temp;
            } else {
                moving = false;
                target = x;
            }
        } else {
            x = pos * RADIUS;
            target = x;
        }

        Point point = map.getPoint(x);
        return new Ellipse2D.Float(point.getX(), point.getY(), RADIUS, RADIUS);
    }

    public Point getPoint() {
        return map.getPoint(x);
    }

    public void setMove(boolean forward, int balls) {
        moving = true;
        if (forward) {
            target += balls * RADIUS;
            pos += balls;
        } else {
            target -= balls * RADIUS;
            pos -= balls;
        }
    }
}
