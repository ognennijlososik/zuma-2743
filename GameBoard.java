import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class GameBoard extends JFrame {

    public static final int BOARD_WIDTH = 800;
    public static final int BOARD_HEIGHT = 600;

    public static boolean leftHeld = false;
    public static boolean rightHeld = false;
    public static boolean spaceHeld = false;
    public static boolean shiftHeld = false;
    public static boolean rHeld = false;
    public static boolean bHeld = false;
    public static boolean cHeld = false;
    public static boolean sHeld = false;


    public static int frame = 0;

    public static void main(String[] args) {
        new GameBoard();
    }

    public GameBoard() {
        this.setSize(BOARD_WIDTH, BOARD_HEIGHT);
        this.setTitle("Zuma");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                //do nothing
            }

            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case 32:
                        spaceHeld = true;
                        break;
                    case 68:
                        rightHeld = true;
                        break;
                    case 65:
                        leftHeld = true;
                        break;
                    case 16:
                        shiftHeld = true;
                        break;
                    case 82:
                        rHeld = true;
                        break;
                    case 66:
                        bHeld = true;
                        break;
                    case 67:
                        cHeld = true;
                        break;
                    case 83:
                        sHeld = true;
                        break;
                    default:
//                        System.out.println(e.getKeyCode());
                        break;
                }
            }

            public void keyReleased(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case 32:
                        spaceHeld = false;
                        break;
                    case 68:
                        rightHeld = false;
                        break;
                    case 65:
                        leftHeld = false;
                        break;
                    case 16:
                        shiftHeld = false;
                        break;
                    case 82:
                        rHeld = false;
                        break;
                    case 66:
                        bHeld = false;
                        break;
                    case 67:
                        cHeld = false;
                        break;
                    case 83:
                        sHeld = false;
                        break;
                }
            }
        });

        GameDrawingPanel gamePanel = new GameDrawingPanel();
        this.add(gamePanel, BorderLayout.CENTER);
        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(5);
        executor.scheduleAtFixedRate(new RepaintTheBoard(this), 0L, 20L, TimeUnit.MILLISECONDS);
        this.setVisible(true);
    }
}

class RepaintTheBoard implements Runnable {

    GameBoard theBoard;

    public RepaintTheBoard(GameBoard theBoard) {
        this.theBoard = theBoard;
    }

    public void run() {
        theBoard.repaint();
        GameBoard.frame++;
    }
}

class GameDrawingPanel extends JComponent {
    private ArrayList<MovingBall> movingBalls;
    private ArrayList<PushedBall> pushedBalls;
    private SimpleColor simpleColor;
    private Map map;

    private Hero hero = new Hero();
    private int lastBallPushed = -1000;
    private int lastButtonPressed = -1000;

    //    private Color nextBallColor;
    private int balls = 20;
    private int colors = 3;
    private int speed = 10;

    private boolean pause = true;
    private boolean win = false;

    public GameDrawingPanel() {
        init();
    }

    private void init() {
        movingBalls = new ArrayList<MovingBall>();
        pushedBalls = new ArrayList<PushedBall>();
        simpleColor = new SimpleColor(colors);
        map = new Map((float) (speed / 10));
        MovingBall.movingBalls = movingBalls;
        MovingBall.map = map;
        PushedBall.pushedBalls = pushedBalls;
        PushedBall.simpleColor = simpleColor;
        PushedBall.map = map;
        for (int i = 0; i < balls; i++) {
            Color next = simpleColor.getRandomColor();
            simpleColor.inGame.add(next);
            movingBalls.add(new MovingBall(i, next));
        }
        simpleColor.updateNextBallColor();

    }

    public void paint(Graphics g) {
        Graphics2D graphicSettings = (Graphics2D) g;
        g.setFont(new Font("TimesRoman", Font.PLAIN, 36));
        graphicSettings.setColor(new Color(250, 250, 250));
        graphicSettings.fillRect(0, 0, getWidth(), getHeight());


        if (pause) {
            graphicSettings.setColor(Color.BLACK);
            if (win) {
                graphicSettings.drawString("YOU WIN!", 200, 150);
            }
            graphicSettings.drawString("(B)alls: " + balls, 200, 250);
            graphicSettings.drawString("(С)olors: " + colors, 200, 300);
            graphicSettings.drawString("(S)peed: " + speed, 200, 350);
            graphicSettings.drawString("STA(R)T!", 200, 450);

            if ((GameBoard.frame - lastButtonPressed > 10) && GameBoard.bHeld) {
                if (GameBoard.shiftHeld && balls > 5) {
                    balls -= 5;
                } else if (!GameBoard.shiftHeld && balls < 30) {
                    balls += 5;
                }
                lastButtonPressed = GameBoard.frame;
            }

            if ((GameBoard.frame - lastButtonPressed > 10) && GameBoard.cHeld) {
                if (GameBoard.shiftHeld && colors > 2) {
                    colors--;
                } else if (!GameBoard.shiftHeld && colors < 5) {
                    colors++;
                }
                lastButtonPressed = GameBoard.frame;
            }

            if ((GameBoard.frame - lastButtonPressed > 10) && GameBoard.sHeld) {
                if (GameBoard.shiftHeld && speed > 10) {
                    speed--;
                } else if (!GameBoard.shiftHeld && speed < 30) {
                    speed++;
                }
                lastButtonPressed = GameBoard.frame;
            }

            if ((GameBoard.frame - lastButtonPressed > 10) && GameBoard.rHeld) {
                pause = false;
                lastButtonPressed = GameBoard.frame;
                init();
            }

            return;
        }

        if (movingBalls.size() == 0) {
            pause = true;
            win = true;
            return;
        }

        if (movingBalls.get(movingBalls.size() - 1).getPoint().getRadius() < 30) {
            map.setLoseSpeed();
        }

        if (map.zeroPoint.getRadius() < 30) {
            pause = true;
            win = false;
            return;
        }

        if (GameBoard.spaceHeld && GameBoard.frame - lastBallPushed > 30) {
            PushedBall pushedBall = new PushedBall(Math.toRadians(hero.getRotationAngle()),
                    simpleColor.nextBallColor);
            pushedBalls.add(pushedBall);
            lastBallPushed = GameBoard.frame;
            simpleColor.updateNextBallColor();
        }

        if (GameBoard.rightHeld && !GameBoard.leftHeld) {
            hero.increaseRotationAngle();
        } else if (GameBoard.leftHeld && !GameBoard.rightHeld) {
            hero.decreaseRotationAngle();
        }

        if ((GameBoard.frame - lastButtonPressed > 10) && GameBoard.rHeld) {
            pause = true;
            win = false;
            lastButtonPressed = GameBoard.frame;
        }

        graphicSettings.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        map.move();
        for (MovingBall movingBall : movingBalls) {
            Ellipse2D ellipse2D = movingBall.getEllipse2D();
            graphicSettings.setPaint(movingBall.color);
            graphicSettings.draw(ellipse2D);
            graphicSettings.fill(ellipse2D);
        }

        for (int i = 0; i < pushedBalls.size(); i++) {
            PushedBall pushedBall = pushedBalls.get(i);
            pushedBall.move();
            Ellipse2D ellipse2D = pushedBall.getEllipse2D();
            graphicSettings.setPaint(pushedBall.color);
            graphicSettings.draw(ellipse2D);
            graphicSettings.fill(ellipse2D);
        }


//        graphicSettings.setTransform(identity);
        graphicSettings.translate(hero.getXCenter(), hero.getYCenter());
        graphicSettings.rotate(Math.toRadians(hero.getRotationAngle()));
        graphicSettings.setPaint(simpleColor.nextBallColor);
        graphicSettings.fill(hero);
//        graphicSettings.setPaint(Color.GRAY);
//        graphicSettings.draw(hero);
    }
}
