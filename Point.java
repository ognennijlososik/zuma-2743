public class Point {
    private final float theta;
    private final float radius;

    public Point(final float theta, final float radius) {
        this.theta = theta;
        this.radius = radius;
    }

    public float getTheta() {
        return theta;
    }

    public float getRadius() {
        return radius;
    }

    public float getX() {
        return GameBoard.BOARD_WIDTH / 2 + (float) (radius * Math.cos(theta));
    }

    public float getY() {
        return GameBoard.BOARD_HEIGHT / 2 + (float) (radius * Math.sin(theta));
    }

}
